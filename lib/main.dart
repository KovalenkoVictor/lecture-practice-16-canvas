import 'package:flutter/material.dart';
import 'dart:math';

void main() => runApp(DemoPageState());

class PieData {
  late Color color;
  late num percentage;
}

class DemoPageState extends StatefulWidget {
  @override
  _DemoPageStateState createState() => _DemoPageStateState();
}

class _DemoPageStateState extends State<DemoPageState> {
  List<PieData> mData=[];

  PieData pieData=PieData();

  @override
  void initState() {
    super.initState();
    initData();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: MyCustomCircle(mData, pieData),
        ),
      ),
    );
  }

  void initData() {
    mData = [];

    double per = 0.14285;

    PieData p1 = PieData();
    p1.percentage = per;
    p1.color = Color(0xff27b4f2);

    mData.add(p1);

    PieData p2 = PieData();

    p2.percentage = per;
    p2.color = Color(0xfffc5c7c);
    mData.add(p2);

    PieData p3 = PieData();

    p3.percentage = per;
    p3.color = Color(0xff00569c);
    mData.add(p3);

    PieData p4 = PieData();

    p4.percentage = per;
    p4.color = Color(0xff6cdc55);
    mData.add(p4);

    PieData p5 = PieData();

    p5.percentage = per;
    p5.color = Color(0xfff7d5a5);
    mData.add(p5);

    PieData p6 = PieData();

    p6.percentage = per;
    p6.color = Color(0xff27b4f2);
    mData.add(p6);

    PieData p7 = PieData();

    p7.percentage = per;
    p7.color = Color(0xfffc5c7c);
    mData.add(p7);
  }
}

class MyView extends CustomPainter {
  Paint _mPaint = Paint();
  double mRadius = 0, mInnerRadius = 0, mBigRadius = 0;
  double mStartAngle = 0;
  Rect mOval = Rect.fromLTWH(0, 0, 0, 0), mBigOval = Rect.fromLTWH(0, 0, 0, 0);

  List<PieData> mData = [];
  PieData pieData =PieData();

  MyView(this.mData , this.pieData);

  @override
  void paint(Canvas canvas, Size size) {
    _mPaint = Paint();
    mRadius = 80.0;
    mBigRadius = 80.0;
    mInnerRadius = mRadius * 0.55;
    mOval = Rect.fromLTRB(-mRadius, -mRadius, mRadius, mRadius);
    mBigOval = Rect.fromLTRB(-mBigRadius, -mBigRadius, mBigRadius, mBigRadius);

    if (mData.length == null || mData.length <= 0) {
      return;
    }

    canvas.save();
    double startAngle = 0.0;
    for (int i = 0; i < mData.length; i++) {
      double sweepAngle = 2 * pi * mData[i].percentage;
      _mPaint..color = mData[i].color;

      int currentSelect = 1;
      if (currentSelect >= 0 && i == currentSelect) {
        canvas.drawArc(mBigOval, startAngle, sweepAngle, true, _mPaint);
      } else {
        canvas.drawArc(mOval, startAngle, sweepAngle, true, _mPaint);
      }
      startAngle += sweepAngle;
    }

    _mPaint..color = Colors.white;
    canvas.drawCircle(Offset.zero, mInnerRadius, _mPaint);
    canvas.restore();
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

// ignore: must_be_immutable
class MyCustomCircle extends StatelessWidget {
  List<PieData> dataList;

  PieData data;
  var dataSize;

  MyCustomCircle(this.dataList, this.data);

  @override
  Widget build(BuildContext context) {
    return CustomPaint(painter: MyView(dataList, data));
  }
}
